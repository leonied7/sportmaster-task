import Axios from 'axios';

class Api {
  constructor() {
    this.axios = Axios.create();
  }

  search(query) {
    return this.axios
      .get('/rest/v1/address', { params: { query } })
      .then((response) => response.data);
  }
}

export default new Api();
