module.exports = {
  lintOnSave: true,
  configureWebpack: {
    devServer: {
      disableHostCheck: true,
      proxy: {
        '/rest': {
          target: process.env.API_PROXY_TARGET
        }
      }
    }
  }
};
